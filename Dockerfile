FROM python:3.7-buster

COPY Pipfile Pipfile.lock ./
COPY api ./api
COPY models ./models

RUN pip install pipenv && \
   pipenv install --skip-lock

EXPOSE 80

CMD ["pipenv", "run", "uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "80"]
