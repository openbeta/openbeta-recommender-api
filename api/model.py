import pandas as pd
from surprise import dump
import joblib as joblib
import numpy as np

df = pd.read_csv("./models/nevada-ratings.zip", compression="zip")

algo = {}
trainset = {}
pred, algo["tacos"] = dump.load("./models/nevada-model-tacos.data")
pred, algo["sushi"] = dump.load("./models/nevada-model-sushi.data")

trainset["tacos"] = joblib.load("./models/nevada-trainset-tacos.data")
trainset["sushi"] = joblib.load("./models/nevada-trainset-sushi.data")

# get similar climbs
# test mp_id = 105732398


def get_neighbors(mp_id, m):
    count = get_rating_count(mp_id)
    m = "tacos"
    if (count < 300):
        m = "sushi"
    try:
        pred = algo[m].get_neighbors(trainset[m].to_inner_iid(mp_id), 50)
        d = map(lambda id: trainset[m].to_raw_iid(id), np.asarray(pred))
        return {"status": 0, "message": "OK",  "data": list(d)}
    except ValueError:
        return {"status": -1, "message": "Invalid climb id {}".format(mp_id), "data": []}
    except Exception as e:
        print(e)
        return {"status": -5, "message": "Unexpected error",  "data": []}


def get_rating_count(climb_id):
   count = (df.route_id == climb_id).sum()
   if count > 0:
    return count + 1
   return 0
