import uvicorn
from fastapi import FastAPI, Query
from starlette.middleware.cors import CORSMiddleware

from api import model

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=['*'])


@app.get("/")
def hello():
    return {"Hello": "World"}


@app.get("/r/{mp_id}")
def get_neighbors(mp_id: int, m: str = Query("tacos", regex="^(tacos|sushi)$")):
    return model.get_neighbors(mp_id, m)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=80)
